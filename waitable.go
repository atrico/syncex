package syncEx

import (
	"context"
)

// An object that can be waited on
type Waitable interface {
	// Wait on this object with a context
	// returns nil if wait returned or error if cancelled/timeout
	Wait(ctx context.Context) error
}

// Waitable that will never return
var InfiniteWaitable Waitable = NewEvent()

// Waitable that will return immediately
var ImmediateWaitable Waitable = func() Waitable { ev := NewEvent(); ev.Set(); return ev }()

// Return object value = Id of waitable triggered
func MergeWaitables(namedObjects map[string]Waitable) WaitableWithValue {
	mw := mergedWaitable{
		make([]string, len(namedObjects)),
		make([]Waitable, len(namedObjects)),
	}
	idx := 0
	for k, v := range namedObjects {
		mw.ids[idx] = k
		mw.waits[idx] = v
		idx++
	}
	return &mw
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type mergedWaitable struct {
	ids   []string
	waits []Waitable
}

func (m *mergedWaitable) WaitForValue(ctx context.Context) (value interface{}, err error) {
	ctx2, cancelWait := context.WithCancel(ctx)
	defer cancelWait()
	select {
	case id := <-mergeWaitable(ctx2, m.waits, m.ids):
		value = id
	case <-ctx.Done():
		err = ctx.Err()
	}
	return value, err
}

func mergeWaitable(ctx context.Context, waits []Waitable, ids []string) (mergedChan chan string) {
	switch len(ids) {
	case 0:
		mergedChan = nil
	case 1:
		mergedChan = waitThenSend(ctx, waits[0], ids[0])
	default:
		mergedChan = make(chan string, 4)
		go func() {
			defer close(mergedChan)
			switch len(ids) {
			case 2:
				select {
				case id := <-waitThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				}
			case 3:
				select {
				case id := <-waitThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				case id := <-waitThenSend(ctx, waits[2], ids[2]):
					mergedChan <- id
				}
			default:
				split := (len(ids) / 2) + 1
				select {
				case id := <-waitThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				case id := <-mergeWaitable(ctx, waits[2:split], ids[2:split]):
					mergedChan <- id
				case id := <-mergeWaitable(ctx, waits[split:], ids[split:]):
					mergedChan <- id
				}
			}
		}()
	}
	return mergedChan
}

func waitThenSend(ctx context.Context, wait Waitable, value string) (channel chan string) {
	channel = make(chan string, 1)
	go func() {
		defer close(channel)
		if wait.Wait(ctx); ctx.Err() == nil {
			channel <- value
		}
	}()
	return channel
}
