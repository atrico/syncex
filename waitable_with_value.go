package syncEx

import (
	"context"
)

// An object that can be waited on to return a value
type WaitableWithValue interface {
	// Wait on this object with a context
	// returns nil if wait returned or error if cancelled/timeout
	// Value set if err == nil
	WaitForValue(ctx context.Context) (value interface{}, err error)
}

// WaitableWithValue that will never return
var InfiniteWaitableWithValue WaitableWithValue = NewPromise()

// WaitableWithValue that will return immediately with nil
var ImmediateWaitableWithValue WaitableWithValue = NewPromise().Fulfil(nil)

type IdAndValue struct {
	Id    string
	Value interface{}
}

// Return object value = IdAndValue, Id of waitable triggered and its value
func MergeWaitablesWithValue(namedObjects map[string]WaitableWithValue) WaitableWithValue {
	mw := mergedWaitableWithValue{
		make([]string, len(namedObjects)),
		make([]WaitableWithValue, len(namedObjects)),
	}
	idx := 0
	for k, v := range namedObjects {
		mw.ids[idx] = k
		mw.waits[idx] = v
		idx++
	}
	return &mw
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type mergedWaitableWithValue struct {
	ids   []string
	waits []WaitableWithValue
}

func (m mergedWaitableWithValue) WaitForValue(ctx context.Context) (value interface{}, err error) {
	ctx2, cancelWait := context.WithCancel(ctx)
	defer cancelWait()
	select {
	case id := <-mergeWaitableWithValue(ctx2, m.waits, m.ids):
		value = id
	case <-ctx.Done():
		err = ctx.Err()
	}
	return value, err
}

func mergeWaitableWithValue(ctx context.Context, waits []WaitableWithValue, ids []string) (mergedChan chan IdAndValue) {
	switch len(ids) {
	case 0:
		mergedChan = nil
	case 1:
		mergedChan = waitForValueThenSend(ctx, waits[0], ids[0])
	default:
		mergedChan = make(chan IdAndValue, 4)

		go func() {
			defer close(mergedChan)
			switch len(ids) {
			case 2:
				select {
				case id := <-waitForValueThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitForValueThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				}
			case 3:
				select {
				case id := <-waitForValueThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitForValueThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				case id := <-waitForValueThenSend(ctx, waits[2], ids[2]):
					mergedChan <- id
				}
			default:
				split := (len(ids) / 2) + 1
				select {
				case id := <-waitForValueThenSend(ctx, waits[0], ids[0]):
					mergedChan <- id
				case id := <-waitForValueThenSend(ctx, waits[1], ids[1]):
					mergedChan <- id
				case id := <-mergeWaitableWithValue(ctx, waits[2:split], ids[2:split]):
					mergedChan <- id
				case id := <-mergeWaitableWithValue(ctx, waits[split:], ids[split:]):
					mergedChan <- id
				}
			}
		}()
	}
	return mergedChan
}

func waitForValueThenSend(ctx context.Context, wait WaitableWithValue, id string) (channel chan IdAndValue) {
	channel = make(chan IdAndValue, 1)
	go func() {
		defer close(channel)
		if val, err := wait.WaitForValue(ctx); err == nil {
			channel <- IdAndValue{id, val}
		}
	}()
	return channel
}
