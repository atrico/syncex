package syncEx

import (
	"context"
	"sync"
)

// Synchronisation event
// Event can be set or reset
// Wait will block until event is set
type Event interface {
	Waitable
	// Is this event set?
	IsSet() bool
	// Set the event
	Set() bool
	// Reset the event
	Reset() bool
	// Set or Reset event based on parameter
	SetValue(value bool) bool
}

func NewEvent() Event {
	return newEvent(false)
}

func NewAutoResetEvent() Event {
	return newEvent(true)
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type event struct {
	autoReset   bool
	set         bool
	accessMutex sync.Mutex
	waitMutex   Mutex
}

func newEvent(autoReset bool) Event {
	ev := event{autoReset, false, sync.Mutex{}, NewMutex()}
	ev.waitMutex.Lock(context.Background())
	return &ev
}

func (e *event) IsSet() bool {
	return e.set
}

func (e *event) Set() bool {
	return e.SetValue(true)
}

func (e *event) Reset() bool {
	return e.SetValue(false)
}

func (e *event) SetValue(value bool) bool {
	e.accessMutex.Lock()
	defer e.accessMutex.Unlock()
	if e.set != value {
		e.set = value
		if value {
			e.waitMutex.Unlock()
		} else {
			e.waitMutex.Lock(context.Background())
		}
		return true
	}
	return false
}

func (e *event) Wait(ctx context.Context) (err error) {
	if err = e.waitMutex.Lock(ctx); err == nil && !e.autoReset {
		e.waitMutex.Unlock()
	}
	return ctx.Err()
}
