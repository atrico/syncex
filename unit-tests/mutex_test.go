package unit_tests

import (
	"context"
	"reflect"
	"testing"
	"time"

	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

var mutexTestTimeout = 100 * time.Millisecond

func Test_Mutex_LockTwice(t *testing.T) {
	for name, locker := range testMutexLockTypes {
		t.Run(name, func(t *testing.T) {
			// Arrange
			mutex := syncEx.NewMutex()
			ctx, _ := context.WithTimeout(context.Background(), mutexTestTimeout)

			// Act
			err1 := locker(ctx, mutex)
			err2 := locker(ctx, mutex)

			// Assert
			assert.That[any](t, err1, is.Nil, "1: no error")
			assert.That[any](t, err2, is.Type(reflect.TypeOf(context.DeadlineExceeded)), "2: timeout")
		})
	}
}

func Test_Mutex_LockThenRelease(t *testing.T) {
	for name, locker := range testMutexLockTypes {
		t.Run(name, func(t *testing.T) {
			// Arrange
			mutex := syncEx.NewMutex()
			ctx, _ := context.WithTimeout(context.Background(), mutexTestTimeout)

			// Act
			err1 := locker(ctx, mutex)
			mutex.Unlock()
			err2 := locker(ctx, mutex)

			// Assert
			assert.That[any](t, err1, is.Nil, "1: no error")
			assert.That[any](t, err2, is.Nil, "2: no error")
		})
	}
}

func Test_Mutex_Race(t *testing.T) {
	for name, locker := range testMutexLockTypes {
		t.Run(name, func(t *testing.T) {
			// Arrange
			mutex := syncEx.NewMutex()
			ctx, _ := context.WithTimeout(context.Background(), mutexTestTimeout)
			waited := false

			// Act
			err1 := locker(ctx, mutex)
			go func() {
				time.Sleep(mutexTestTimeout / 2)
				waited = true
				mutex.Unlock()
			}()
			err2 := locker(ctx, mutex)

			// Assert
			assert.That[any](t, err1, is.Nil, "1: no error")
			assert.That[any](t, err2, is.Nil, "2: no error")
			assert.That(t, waited, is.True, "go-routine executed")
		})
	}
}

var testMutexLockTypes = map[string]func(ctx context.Context, m syncEx.Mutex) error{
	"Lock": func(ctx context.Context, m syncEx.Mutex) error { return m.Lock(ctx) },
	"Wait": func(ctx context.Context, m syncEx.Mutex) error { return m.Wait(ctx) },
}
