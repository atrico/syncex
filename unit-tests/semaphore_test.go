package unit_tests

import (
	"context"
	"fmt"
	"testing"

	"gitlab.com/atrico/syncEx"
	. "gitlab.com/atrico/testing/v2"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_Semaphore(t *testing.T) {
	// Arrange
	semaphore := syncEx.NewSemaphore(2)

	// Act
	result1 := semaphore.Wait(createContext())
	result2 := semaphore.Wait(createContext())
	result3 := semaphore.Wait(createContext())

	// Assert
	assert.That[any](t, result1, is.Nil, "1st Wait ok")
	assert.That[any](t, result2, is.Nil, "2nd Wait ok")
	assert.That(t, result3, is.EqualTo(context.DeadlineExceeded), "3rd Wait timeout")
}

func Test_Semaphore_Release(t *testing.T) {
	// Arrange
	semaphore := syncEx.NewSemaphore(2)

	// Act
	result1 := semaphore.Wait(createContext())
	result2 := semaphore.Wait(createContext())
	semaphore.Release()
	result3 := semaphore.Wait(createContext())
	result4 := semaphore.Wait(createContext())

	// Assert
	assert.That[any](t, result1, is.Nil, "1st Wait ok")
	assert.That[any](t, result2, is.Nil, "2nd Wait ok")
	assert.That[any](t, result3, is.Nil, "3rd Wait ok")
	assert.That(t, result4, is.EqualTo(context.DeadlineExceeded), "4th Wait timeout")
}

func Test_Semaphore_Status(t *testing.T) {
	// Arrange
	semaphore := syncEx.NewSemaphore(2)
	var current [6]int
	var limit [6]int

	// Act
	current[0], limit[0] = semaphore.Status()
	semaphore.Wait(createContext())
	current[1], limit[1] = semaphore.Status()
	semaphore.Wait(createContext())
	current[2], limit[2] = semaphore.Status()
	semaphore.Wait(createContext())
	current[3], limit[3] = semaphore.Status()
	semaphore.Release()
	current[4], limit[4] = semaphore.Status()
	semaphore.Release()
	current[5], limit[5] = semaphore.Status()

	// Assert
	assert.That(t, current[0], is.EqualTo(0), "Initial")
	assert.That(t, current[1], is.EqualTo(1), "After 1 wait")
	assert.That(t, current[2], is.EqualTo(2), "After 2 waits")
	assert.That(t, current[3], is.EqualTo(2), "After 2 waits, 1 timeout")
	assert.That(t, current[4], is.EqualTo(1), "After 2 waits, 1 release")
	assert.That(t, current[5], is.EqualTo(0), "After 2 waits, 2 releases")
	for i, lim := range limit {
		assert.That(t, lim, is.EqualTo(2), "Limit %d", i)
	}
}

func Test_Semaphore_OverRelease(t *testing.T) {
	// Arrange
	semaphore := syncEx.NewSemaphore(2)

	// Act
	semaphore.Wait(createContext())
	semaphore.Wait(createContext())
	semaphore.Release()
	semaphore.Release()
	err := PanicCatcher(semaphore.Release)
	fmt.Println(err)

	// Assert
	assert.That(t, err, is.NotNil, "panic")
}
