package unit_tests

import (
	"context"
	"os"
	"testing"
	"time"

	"github.com/pkg/errors"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"github.com/rs/zerolog/pkgerrors"
	"gitlab.com/atrico/testing/v2/random"
	"go.uber.org/goleak"
)

var rg = random.NewValueGenerator()

func TestMain(m *testing.M) {
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	zerolog.ErrorStackMarshaler = pkgerrors.MarshalStack
	zerolog.SetGlobalLevel(zerolog.TraceLevel)
	goleak.VerifyTestMain(m)
}

var timeoutError = errors.New("timeout")

func createContext() context.Context {
	timeout := 250 * time.Millisecond
	ctx, _ := context.WithTimeout(context.Background(), timeout)
	return ctx
}
