package unit_tests

import (
	"context"
	"reflect"
	"sync"
	"testing"
	"time"

	"gitlab.com/atrico/syncEx"
	"gitlab.com/atrico/testing/v2/assert"
	"gitlab.com/atrico/testing/v2/is"
)

func Test_WithContext_Success(t *testing.T) {
	// Arrange
	ctx, _ := context.WithTimeout(context.Background(), time.Second)

	// Act
	err := syncEx.WithContext(func() {}, ctx)

	// Assert
	assert.That[any](t, err, is.Nil, "Succeeded")
}

func Test_WithContext_Timeout(t *testing.T) {
	// Arrange
	mut := sync.Mutex{}
	mut.Lock()
	defer mut.Unlock()
	ctx, _ := context.WithTimeout(context.Background(), 200*time.Millisecond)

	// Act
	err := syncEx.WithContext(func() { mut.Lock() }, ctx)

	// Assert
	assert.That[any](t, err, is.Type(reflect.TypeOf(context.DeadlineExceeded)), "Timeout")
}
