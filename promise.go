package syncEx

import (
	"context"
)

type Promise interface {
	Waitable
	WaitableWithValue
	// Set the value and fulfil the promise
	Fulfil(value interface{}) Promise
}

func NewPromise() Promise {
	return &promise{NewEvent(), nil}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------

type promise struct {
	done  Event
	value interface{}
}

func (p *promise) Wait(ctx context.Context) (err error) {
	return p.done.Wait(ctx)
}

func (p *promise) WaitForValue(ctx context.Context) (value interface{}, err error) {
	if err = p.done.Wait(ctx); err == nil {
		value = p.value
	}
	return value, err
}

func (p *promise) Fulfil(value interface{}) Promise {
	p.value = value
	p.done.Set()
	return p
}
