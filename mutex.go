package syncEx

import (
	"context"
)

type Mutex interface {
	Waitable
	// Lock the mutex (synonym for Wait())
	Lock(ctx context.Context) error
	// Unlock the mutex
	Unlock()
}

func NewMutex() Mutex {
	return &mutex{make(chan struct{}, 1)}
}

// ----------------------------------------------------------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------------------------------------------------------
type mutex struct {
	ch chan struct{}
}

func (m *mutex) Wait(ctx context.Context) error {
	return m.Lock(ctx)
}

func (m *mutex) Lock(ctx context.Context) error {
	select {
	case m.ch <- struct{}{}:
	case <-ctx.Done():
	}
	return ctx.Err()
}

func (m *mutex) Unlock() {
	<-m.ch
}
