package syncEx

import (
	"context"
	"time"
)

type ContextWithValues interface {
	context.Context
	AddValue(key, value any) ContextWithValues
	AddValues(values map[any]any) ContextWithValues
}

func NewContextWithValues(ctx context.Context) ContextWithValues {
	return contextWithValues{ctx, make(map[any]any)}
}
func NewBackgroundContextWithValues() ContextWithValues {
	return NewContextWithValues(context.Background())
}

// ----------------------------------------------------------------------------------------------------------------------------
// Context with values
// ----------------------------------------------------------------------------------------------------------------------------
type contextWithValues struct {
	ctx    context.Context
	values map[any]any
}

// Forward to underlying context
func (c contextWithValues) Deadline() (deadline time.Time, ok bool) {
	return c.ctx.Deadline()
}
func (c contextWithValues) Done() <-chan struct{} {
	return c.ctx.Done()
}
func (c contextWithValues) Err() error {
	return c.ctx.Err()
}

// Values
func (c contextWithValues) Value(key any) (value any) {
	var ok bool
	if value, ok = c.values[key.(string)]; !ok {
		value = nil
	}
	return
}
func (c contextWithValues) AddValue(key, value any) ContextWithValues {
	c.values[key] = value
	return c
}

func (c contextWithValues) AddValues(values map[any]any) ContextWithValues {
	for k, v := range values {
		c.values[k] = v
	}
	return c
}
