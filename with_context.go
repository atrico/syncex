package syncEx

import (
	"context"
)

// Perform action with context (cancel and/or timeout)
// return nil if action completed, error if timed out/cancelled
// If action never completes, goroutine will be leaked
func WithContext(action func(), ctx context.Context) error {
	c := make(chan struct{})
	go func() {
		defer close(c)
		action()
	}()
	select {
	case <-c:
		return nil // completed normally
	case <-ctx.Done():
		return ctx.Err() // timed out/cancelled
	}
}
